package com.aniket.SpringJpaRestConcurrency;

public interface DBService {

    public StudentWithVersionCheck updateStudent();

    StudentWithVersionCheck updateStudentWithoutVersion();
}
