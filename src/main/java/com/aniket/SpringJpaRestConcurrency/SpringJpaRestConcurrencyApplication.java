package com.aniket.SpringJpaRestConcurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaRestConcurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaRestConcurrencyApplication.class, args);
	}

}
