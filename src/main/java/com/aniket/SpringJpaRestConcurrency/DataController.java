package com.aniket.SpringJpaRestConcurrency;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@Slf4j
public class DataController {

    @Autowired
    private DBService dbService;

    @GetMapping("/")
    public Response updateData(){
        try {
            //StudentWithVersionCheck studentWithVersionCheck = dbService.updateStudent();
            StudentWithVersionCheck studentWithVersionCheck = dbService.updateStudentWithoutVersion();
            return Response.builder()
                    .status("All good")
                    .studentWithVersionCheck(studentWithVersionCheck)
                    .dateUpdated(new Date())
                    .threadInfo(Thread.currentThread().toString()).build();
        }catch (Exception exception){
            log.error("--Error -- {}",exception);
            return Response.builder()
                    .status("not good - "+exception.getClass())
                    .errorMessage(exception.getMessage())
                    .dateUpdated(new Date())
                    .threadInfo(Thread.currentThread().toString()).build();

        }
    }

}
