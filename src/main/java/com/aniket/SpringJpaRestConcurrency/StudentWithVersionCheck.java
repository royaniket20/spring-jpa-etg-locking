package com.aniket.SpringJpaRestConcurrency;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Student")
public class StudentWithVersionCheck {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column
    private String id;
    @Column
    private String first_name;
    @Column
    private String last_name;
    @Column
    private String email;
    @Column
    private String gender;

    @Version
    private long version;

}
/**
 * create table Student (
 * 	id VARCHAR(40),
 * 	first_name VARCHAR(50),
 * 	last_name VARCHAR(50),
 * 	email VARCHAR(50),
 * 	gender VARCHAR(50)
 * );
 */
