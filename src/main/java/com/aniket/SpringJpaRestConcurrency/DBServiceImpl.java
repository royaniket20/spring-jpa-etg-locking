package com.aniket.SpringJpaRestConcurrency;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DBServiceImpl implements  DBService{

    @Autowired
    private DBServiceRepo dbServiceRepo;
    //TODO - Read https://thorben-janssen.com/spring-data-jpa-state-detection/
    //TODO - Read - https://bartslota.com/concurrency-control-in-rest-api-wi/
    //Version number will ensure that - what you read is same what you are updating
    //Even If appliction Running in two Different JVMs parallely - Still it will work as Its happening at DB level
    @Override
    public StudentWithVersionCheck updateStudent() {
        String id = "a49bc617-ce4f-44c0-ad2c-cf71fd4f9a2d";
        StudentWithVersionCheck curr = dbServiceRepo.findById(id).get();
        log.info(" Fetched Student --- by Current Thread {} ---> {}",Thread.currentThread() , new Gson().toJson(curr));
        if(curr.getGender().equals("Female")){
            curr.setGender("Male");
            log.info("Made Female to Male Conversion by Thread - {}",Thread.currentThread());
        }else{
            curr.setGender("Female");
            log.info("Made Male to Female Conversion by Thread - {}",Thread.currentThread());
        }
        dbServiceRepo.save(curr);
        StudentWithVersionCheck recent = dbServiceRepo.findById(id).get();
        log.info(" Fetched Student Again---> by Current Thread {} ---> {}",Thread.currentThread() , new Gson().toJson(recent));
        return recent;
    }


    @Override
    public StudentWithVersionCheck updateStudentWithoutVersion() {
        String id = "a49bc617-ce4f-44c0-ad2c-cf71fd4f9a2d";
        StudentWithVersionCheck curr = dbServiceRepo.findById(id).get();
        StudentWithVersionCheck recent = new StudentWithVersionCheck();
        BeanUtils.copyProperties(curr,recent);
        log.info(" Fetched Student --- by Current Thread {} ---> {}",Thread.currentThread() , new Gson().toJson(curr));
        if(recent.getGender().equals("Female")){
            recent.setGender("Male");
            log.info("Made Female to Male Conversion by Thread - {}",Thread.currentThread());
        }else{
            recent.setGender("Female");
            log.info("Made Male to Female Conversion by Thread - {}",Thread.currentThread());
        }
        dbServiceRepo.save(recent);
        StudentWithVersionCheck last = dbServiceRepo.findById(id).get();
        log.info(" Fetched Student Again---> by Current Thread {} ---> {}",Thread.currentThread() , new Gson().toJson(last));
        return last;
    }
}
