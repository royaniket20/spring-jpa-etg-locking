package com.aniket.SpringJpaRestConcurrency;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class Response {
    private String status;
    private StudentWithVersionCheck studentWithVersionCheck;
    private String errorMessage;
    private Date dateUpdated;
    private String threadInfo;

}
