package com.aniket.SpringJpaRestConcurrency;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBServiceRepo extends CrudRepository<StudentWithVersionCheck,String> {

}
